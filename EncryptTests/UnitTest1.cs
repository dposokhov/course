﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CourseProject;
using System.Windows.Forms;

namespace EncryptTests
{
    [TestClass]
    public class CipherTest
    {
        [TestMethod]
        public void GetNumber_45()
        {
            //Arrange
            TextBox textBox = new TextBox();
            textBox.Text = "45";
            int expected = 45;
            
            //Act
            int answer = Form1.GetNumber(textBox,false);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNumber_qwerty()
        {
            //Arrange
            TextBox textBox = new TextBox();
            textBox.Text = "qwerty";
            int expected = 0;

            //Act
            int answer = Form1.GetNumber(textBox, false);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNumber_empty()
        {
            //Arrange
            TextBox textBox = new TextBox();
            textBox.Text = "";
            int expected = 0;

            //Act
            int answer = Form1.GetNumber(textBox, false);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNumber_minus_2()
        {
            //Arrange
            TextBox textBox = new TextBox();
            textBox.Text = "-2";
            int expected = 0;

            //Act
            int answer = Form1.GetNumber(textBox, false);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNumber_123456q()
        {
            //Arrange
            TextBox textBox = new TextBox();
            textBox.Text = "123456q";
            int expected = 0;

            //Act
            int answer = Form1.GetNumber(textBox, false);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText1()
        {
            //Arrange
            bool goForward = true;
            int countOfletters = 30;
            string s = "Обычная строка";
            string expected = "Люшфкэь опнлзэ";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText2()
        {
            //Arrange
            bool goForward = true;
            int countOfletters = 30;
            string s = "Обычная строка and something";
            string expected = "Люшфкэь опнлзэ and something";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText3()
        {
            //Arrange
            bool goForward = true;
            int countOfletters = 30;
            string s = "";
            string expected = "";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText4()
        {
            //Arrange
            bool goForward = true;
            int countOfletters = 30;
            string s = "1q2w3e4r5t6!@#$%^&/.";
            string expected = "1q2w3e4r5t6!@#$%^&/.";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText5()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 30;
            string s = "Обыsчная строка";
            string expected = "Сдюsъргв фхуснг";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText6()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 30;
            string s = "Обычная строка";
            string expected = "Сдюъргв фхуснг";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void GetNewText7()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 2;
            string s = "Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!\nД сткпшксж српбфю, щфр фхф кусрнюйхжфуб ъкцт Шжйвтб пж рургр фтхёпр, рупрдпвб йведрйёмв уруфркф д фро, щфргэ ргэетвфю увох укфхвшка у ёжъкцтрдмрл к рстжёжнжпкжо пвствднжпкб к ъвев уёдкев.Фжсжтю ёжнр руфвнрую йв овнэо, ёрёжнвфю стретвоох ёр нрекщжумрер мрпшв, дэсрнпкфю дуж хунрдкб йвёвпкб к рсхгнкмрдвфю удра твгрфх!Орнрёжш, яфр гэнк ёруфвфрщпр фтхёпэж к кпфжтжупэж ёдв у срнрдкпрл ожубшв, пр дсжтжёк пву иёжф жыж оприжуфдр рфмтэфкл, к б пвёжаую ргыкч уджтъжпкл!\nРф нкшв мросвпкк FirstLineSoftware к Хпкджтукфжфв КФОР, б твё срйётвдкфю фжгб у рцкшквнюпэо рмрпщвпкжо пвъкч мхтурд У# ёнб пвщкпваыкч! Оэ чрфко срижнвфю хусжчрд д ёвнюпжлъжо сретхижпкк д окт КФ к стретвооктрдвпкб у кусрнюйрдвпкжо уфжмв фжчпрнрекл .Net, ортж фжтсжпкб к кпфжтжупэч йвёвщ!";
            string expected = "Поздравляю, ты получил исходный текст!!!\nВ принципе понять, что тут используется шифр Цезаря не особо трудно, основная загвоздка состоит в том, чтобы обыграть саму ситуацию с дешифровкой и определением направления и шага сдвига.Теперь дело осталось за малым, доделать программу до логического конца, выполнить все условия задания и опубликовать свою работу!Молодец, это были достаточно трудные и интересные два с половиной месяца, но впереди нас ждет еще множество открытий, и я надеюсь общих свершений!\nОт лица компании FirstLineSoftware и Университета ИТМО, я рад поздравить тебя с официальным окончанием наших курсов С# для начинающих! Мы хотим пожелать успехов в дальнейшем погружении в мир ИТ и программирования с использованием стека технологий .Net, море терпения и интересных задач!";


            //Act
            string answer = Form1.GetNewText(s, goForward, countOfletters);

            //Assert
            Assert.AreEqual(expected, answer);

        }
        [TestMethod]
        public void CreateDocxAndGetTextFrom1()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 30;
            string path = Environment.CurrentDirectory + "//temp.docx";
            string text = "Обычная строка";


            //Act
            Form1.CreateNewDocx(path, text);

            //Assert
            Assert.AreEqual(Form1.GetTextFromDocx(path), text);
            System.IO.File.Delete(path);
        }
        [TestMethod]
        public void CreateDocxAndGetTextFrom2()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 30;
            string path = Environment.CurrentDirectory + "//temp.docx";
            string text = "Обычная строкаqewgaerhn!@$#@$^475" +
                "\n\t\naerhaernztrntrnjWEGRFXBS1234567";


            //Act
            Form1.CreateNewDocx(path, text);

            //Assert
            Assert.AreEqual(Form1.GetTextFromDocx(path), text);
            System.IO.File.Delete(path);
        }
        [TestMethod]
        public void CreateDocxAndGetTextFrom3()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 30;
            string path = Environment.CurrentDirectory + "//temp.docx";
            string text = "";


            //Act
            Form1.CreateNewDocx(path, text);

            //Assert
            Assert.AreEqual(Form1.GetTextFromDocx(path), text);
            System.IO.File.Delete(path);
        }
        [TestMethod]
        public void CreateDocxAndGetTextFrom4()
        {
            //Arrange
            bool goForward = false;
            int countOfletters = 30;
            string path = Environment.CurrentDirectory + "//temp.docx";
            string text = "qsdgbfdQFDGEWSGSE123456789765432ccvcvcvcc";


            //Act
            Form1.CreateNewDocx(path, text);

            //Assert
            Assert.AreEqual(Form1.GetTextFromDocx(path), text);
            System.IO.File.Delete(path);
        }
        [TestMethod]
        public void Reverse1()
        {
            //Arrange
            string s = "1234567";
            string result = "7654321";

            //Act
            string answer = Form1.Reverse(s);

            //Assert
            Assert.AreEqual(result, answer);
        }
        [TestMethod]
        public void Reverse2()
        {
            //Arrange
            string s = "Azxcv";
            string result = "vc45zA";

            //Act
            string answer = Form1.Reverse(s);

            //Assert
            Assert.AreNotEqual(result, answer);
        }
    }
}
