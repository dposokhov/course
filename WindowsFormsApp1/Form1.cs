﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;


namespace CourseProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static string alphabetL = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        public static string alphabetU = alphabetL.ToUpper();
        public static string ralphabetL = Reverse(alphabetL);
        public static string ralphabetU = Reverse(alphabetU);
        public static bool goForward = true;
        public static bool isRadioOn = false;        
        public static int countOfletters = 0;
        public static bool goForwardTest = true;
        public static bool isRadioOnTest = false;
        public static int countOflettersTest = 0;

        //Шифратор---------------------------------------------------------------------
        public void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
            {
                goForward = true;
                isRadioOn = true;
                MessageBox.Show("Направление установлено");
            }
            
        }

        public void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
            {
                goForward = false;
                isRadioOn = true;
                MessageBox.Show("Направление установлено");
            }
        }

        public void button1_Click(object sender, EventArgs e)
        {
            OpenDocument(textBox1, openFileDialog1);
        }

        public void button3_Click(object sender, EventArgs e)
        {
            if (countOfletters != 0 && isRadioOn == true && textBox1.Text != "")
            {
                textBox3.Text = GetNewText(textBox1.Text,goForward,countOfletters);
            }
            else
            {
                MessageBox.Show("Проверьте: \nВведен ли шаг \nНаписана строка или загружен файл \nВыбрано направление","Недостаточно данных");
            }
        }

        public void button7_Click(object sender, EventArgs e)
        {
            countOfletters = GetNumber(textBox5);
        }

        public void button2_Click(object sender, EventArgs e)
        {
            SaveDocument(textBox3, saveFileDialog1);
        }

        //Конец шифратора______________________________________________________________________________________


        //Тестовый дешифратор----------------------------------------------------------------------------------
        public void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                goForwardTest = true;
                isRadioOnTest = true;
                MessageBox.Show("Направление установлено");
            }
        }

        public void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
            {
                goForwardTest = false;
                isRadioOnTest = true;
            }
        }

        public void button6_Click(object sender, EventArgs e)
        {
            OpenDocument(textBox2, openFileDialog1);
        }

        public void button4_Click(object sender, EventArgs e)
        {
            countOflettersTest = GetNumber(textBox6, false);
            if (countOflettersTest != 0 && isRadioOnTest == true && textBox2.Text != "")
            {
                textBox4.Text = GetNewText(textBox2.Text,goForwardTest,countOflettersTest);
            }
            else
            {
                MessageBox.Show("Проверьте: \nВведен ли шаг \nНаписана строка или загружен файл \nВыбрано направление", "Недостаточно данных");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            countOflettersTest = GetNumber(textBox6);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SaveDocument(textBox4, saveFileDialog1);
        }

        //Конец тестового дешифратора___________________________________________________________________-


        public static void OpenDocument(TextBox textBox, OpenFileDialog openFileDialog)
        {
            if (openFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return;
            }
            string filename = openFileDialog.FileName;

            switch (openFileDialog.FilterIndex)
            {
                case 1:
                    {
                        try
                        {
                            string fileText;                            
                            FileStream fs = (FileStream)openFileDialog.OpenFile();
                            using (StreamReader sr = new StreamReader(fs, System.Text.Encoding.UTF8))
                            {
                                fileText = sr.ReadToEnd();
                                textBox.Text = fileText;
                                MessageBox.Show("Файл txt открыт");                                
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Исключение");
                            break;
                        }
                    }
                case 2:
                    {
                        try
                        {
                            string fileText;
                            FileStream fs = (FileStream)openFileDialog.OpenFile();
                            using (StreamReader sr = new StreamReader(fs, System.Text.Encoding.GetEncoding(1251)))
                            {
                                fileText = sr.ReadToEnd();
                                textBox.Text = fileText;
                                MessageBox.Show("Файл txt открыт");                                
                            }
                            break;
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Исключение");
                            break;
                        }
                    }
                case 3:
                    {
                        try
                        {
                            string fileText = "";
                            fileText = GetTextFromDocx(filename);
                            textBox.Text = fileText;
                            MessageBox.Show("Файл docx открыт");
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Исключение");
                        }
                        break;
                    }
            }
        }


        public static string GetNewText(string s, bool goForward, int countOfletters)
        {
            string end = "";
            foreach (var item in s)
            {
                if ( !(alphabetL.Contains(item.ToString()) || alphabetU.Contains(item.ToString())) )
                {
                    end += item;
                }
                else
                {
                    string currentAlphabet;
                    if (alphabetL.Contains(item.ToString()) && goForward == true)
                    {
                        currentAlphabet = alphabetL;
                    }
                    else if (alphabetL.Contains(item.ToString()) && goForward == false)
                    {
                        currentAlphabet = ralphabetL;
                    }
                    else if (alphabetU.Contains(item.ToString()) && goForward == true)
                    {
                        currentAlphabet = alphabetU;
                    }
                    else 
                    {
                        currentAlphabet = ralphabetU;
                    }

                    int currentIndex = currentAlphabet.IndexOf(item);

                    if (currentIndex + countOfletters <= currentAlphabet.Length - 1)
                    {
                        end += currentAlphabet[currentIndex + countOfletters];
                    }
                    else
                    {
                        int newIndex = (currentIndex + countOfletters) % (currentAlphabet.Length);
                        end += currentAlphabet[newIndex];
                    }
                }
                

            }
            return end;
        }
        public static string Reverse(string input)
        {
            return new string(input.ToCharArray().Reverse().ToArray());
        }



        public static void SaveDocument(TextBox textBox, SaveFileDialog saveFileDialog)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.Cancel)
            {
                return ;
            }
            string filename = saveFileDialog.FileName;

            if (filename != "")
            {
                switch (saveFileDialog.FilterIndex)
                {
                    case 1:
                        {
                            using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.UTF8))
                            {
                                sw.Write(textBox.Text);
                            }                                
                            break;
                        }
                    case 2:
                        {
                            using (StreamWriter sw = new StreamWriter(filename, false, System.Text.Encoding.GetEncoding(1251)))
                            {
                                sw.Write(textBox.Text);
                            }
                            break;
                        }
                    case 3:
                        {
                            CreateNewDocx(filename, textBox.Text);
                            break;
                        }
                }
            }
        }
        public static string GetTextFromDocx(string path)
        {
            try
            {
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(path, true))
                {
                    var text = wordDoc.MainDocumentPart.Document.InnerText;
                    return text;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void CreateNewDocx(string path, string text)
        {
            try
            {
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Create(path, 0))
                {
                    MainDocumentPart mainPart = wordDoc.AddMainDocumentPart();
                    new Document(new Body()).Save(mainPart);

                    Body body = mainPart.Document.Body;
                    body.Append(new Paragraph(
                       new Run(
                        new Text(text))));

                    mainPart.Document.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static int GetNumber(TextBox textBox, bool flag=true)
        {
            try
            {
                int value = Int32.Parse(textBox.Text);
                if (value > 0)
                {
                    
                    if (flag)
                    {
                        MessageBox.Show("Шаг установлен");
                    }
                    return value;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                if (flag)
                {
                    MessageBox.Show("Введите целое число большее нуля");
                }
            }
            return 0;
        }
    }
}
